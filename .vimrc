set number	"numbers of strings"
colorscheme	darkblue
let mapleader = ","	"first button for personal keybindings"
map <space> /
"control-c, control-v keybindings
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y
vnoremap <C-d> "+d
"--------------------------------
set hlsearch "bachlight for search
set encoding=utf8	"encoding
"comment biindings

